# HTOP Cluster


![](ezgif.com-video-to-gif_1_.gif)



Launches an instance of htop on each node of your cluster.

## Useful if:
1. You (the user) can ssh into the cluster compute nodes.

2. You (the user) do not have access to cluster monitoring tools like Ganglia, Zabbix etc.

3. You (the frustrated user) want to see if other users are using the prescribed job scheduler (e.g Slurm)  
or if they are just running jobs directly on nodes (by abusing ssh access to compute nodes).

## Background and Motivation
This probably won't work on your cluster because users shouldn't have direct  
ssh access to compute nodes. This was not the case on the cluster at my university,  
hence the need arose to ensure my jobs ran unimpeded. Using tools like Ganglia  
required sudo privileges to install (I tried), so this little script was my solution.  
It also looks really cool.


![](nice.mov)


## Prerequisites
You will have to install the following dependencies yourself (follow the instructions in the links):
* xpanes: https://github.com/greymd/tmux-xpanes
* nvtop: https://github.com/Syllo/nvtop

## Customising
You will have to edit the python script according to your needs.  
That means changing the host names to reflect your cluster.  
In my case each node is given the host name ` 'mscluster' + 'node number' `.  
For example: the first node is called `mscluster1`, the second is `mscluster2` etc.  
The cluster I'm using divides its 73 nodes into partitions:  
* batch: For general purpose use. Up to 60 nodes each with an i7 7700 CPU, GTX1060 6GB GPU, and 16GB of RAM.

* ha: For high priority runs when you anticipate [load shedding](https://en.wikipedia.org/wiki/2007-2019_South_African_energy_crisis "wtf is load shedding? click to find out.") may affect your work. Up to 10 nodes each with an i7 7700 CPU, GTX1060 6GB GPU, and 16GB of RAM.

* biggpu: For jobs requiring more that 8GB of GPU RAM. Up to 3 nodes, two with a GTX1080, and one with a GTX1080ti.

## Usage

### Run:
`$ monitor [options]`

### Options: (edit these to the names of your cluster partitions in the script)
* `ha` 
* `gpu`
* `batch`

### Example: to monitor the ha partition:
* `$ monitor ha`

### Stop:
1. hit **ctrl + C**
2. then type `exit`


